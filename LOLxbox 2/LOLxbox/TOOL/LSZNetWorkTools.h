//
//  LSZNetWorkTools.h
//  Heart Fire
//
//  Created by mac on 15/12/28.
//  Copyright © 2015年 TUcheng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

//请求成功block类型
typedef void(^SuccessBlock)(id);
//请求失败block类型
typedef void(^FailBlock)(NSError *);
/**
 *  返回值类型
 */
typedef NS_ENUM(NSUInteger, ResponseType) {
    /**
     *  JSON类型
     */
    ResponseTypeJSON,
    /**
     *  XML类型
     */
    ResponseTypeXML,
    /**
     *  DATA类型
     */
    ResponseTypeDATA,
};


typedef NS_ENUM(NSUInteger, BodyType) {
    BodyTypeString,
    BodyTypeDictionary,
};



@interface LSZNetWorkTools : NSObject

/**
 *  get请求
 *
 *  @param url          URL
 *  @param parameter    参数
 *  @param header       请求头
 *  @param responseType 返回值类型
 *  @param success      请求成功
 *  @param fail         请求失败
 */
+ (void)getWithUrl:(NSString *)url parameter:(NSDictionary *)parameter httpHeader:(NSDictionary *)header resposeType:(ResponseType)responseType success:(SuccessBlock)success fail:(FailBlock)fail;



/**
 *  post请求
 *
 *  @param url         URL
 *  @param body        body体
 *  @param bodyType    body类型
 *  @param header      请求头
 *  @param reponseType 返回值类型
 *  @param success     请求成功
 *  @param fail        请求失败
 */
+ (void)postWithURL:(NSString *)url body:(id)body bodyType:(BodyType)bodyType httpHeader:(NSDictionary *)header responseType:(ResponseType)responseType success:(SuccessBlock)success fail:(FailBlock)fail;

@end
