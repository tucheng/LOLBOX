//
//  AppDelegate.h
//  LOLxbox
//
//  Created by mac on 16/1/27.
//  Copyright © 2016年 Grasstrio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

